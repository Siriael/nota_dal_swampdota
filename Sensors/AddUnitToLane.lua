

local sensorInfo = {
	name = "CenterMass",
	desc = "Return Center Mass of a groupUnits ",
	author = "DarioLanza",
	date = "2010-04-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

--************************************************     PriorityQueue     **************************************************************

local SpringGetUnitPosition = Spring.GetUnitPosition


return function( unitbought, UnitsBeforeBuying )
	
	local  UnitsAfterBuying = Spring.GetTeamUnits(Spring.GetMyTeamID())
	for index, unitID in pairs(UnitsAfterBuying)do
		if( UnitsBeforeBuying[unitID] == nil )then
			unitbought.id = unitID
			return unitbought
		end	
	end
	
end





