local sensorInfo = {
	name = "myPosition",
	desc = "Return position of the point unit.",
	author = "DarioLanza",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetUnitHealth = Spring.GetUnitHealth
-- @argument unitID number
-- @description return static position of the first unit
return function(listOfUnits)
	local newListOfUnits = {}
	local farks = {}
	local armmavs = {}
	local armmarts = {}
	local radars = {}
	local misc = {}
	local wounded = {}
	local zeus = {}
	local peeps = {}
	local atlas = {}
	local boxes = {}
	for index,thisUnitID in pairs (listOfUnits) do
		
		local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
	
		local name = UnitDefs[thisUnitDefID].name
		
		local actualhealth,maxhealth,_,_,_ = SpringGetUnitHealth(thisUnitID)
		
		--if(  actualhealth <= 60*maxhealth/100)then
		--	
		--		wounded[#wounded + 1] = thisUnitID
		--else
			if (UnitDefs[thisUnitDefID].name == "armfark") then -- in category
				farks[#farks + 1] = thisUnitID
			else
				if (UnitDefs[thisUnitDefID].name == "armmav") then -- in category
					armmavs[#armmavs + 1] = thisUnitID
				else
					if (UnitDefs[thisUnitDefID].name == "armmart") then -- in category
						armmarts[#armmarts + 1] = thisUnitID
					else	
						if (UnitDefs[thisUnitDefID].name == "armseer") then -- in category
							radars[#radars + 1] = thisUnitID
						else	
							if (UnitDefs[thisUnitDefID].name == "armzeus") then -- in category
								zeus[#zeus + 1] = thisUnitID
							else	
								if (UnitDefs[thisUnitDefID].name == "armpeep") then -- in category
									peeps[#peeps + 1] = thisUnitID
								else	
									if (UnitDefs[thisUnitDefID].name == "armatlas") then -- in category
										atlas[#atlas + 1] = thisUnitID
									else	
										if (UnitDefs[thisUnitDefID].name == "armbox") then -- in category
											boxes[#boxes + 1] = thisUnitID
										else	
											misc[#misc +1 ]= thisUnitID
										end
									end
								end
							end
						end
					end
				end 
			end 
		end
	--end
	
	 local ListOfGroup = { misc = misc , farks = farks , armmavs = armmavs , armmarts = armmarts , radars = radars, wounded = wounded,zeus = zeus,peeps = peeps,atlas = atlas,boxes = boxes}
		return ListOfGroup
	--Spring.Echo(dist)
end
