local sensorInfo = {
	name = "myPosition",
	desc = "Return position of the point unit.",
	author = "DarioLanza",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
-- @argument unitID number
-- @description return static position of the first unit
return function(point_1,point_2)
	local res = Vec3( ( point_1.x+point_2.x)/2 , ( point_1.y+point_2.y)/2 , ( point_1.z+point_2.z)/2)
	
	return res
end
