local sensorInfo = {
	name = "myDebug",
	desc = "Sends data to example debug widget",
	author = "Dario Lanza",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function()
	local teams = Spring.GetTeamList()
	local myTeamID = Spring.GetMyTeamID()
	local gaiaID = Spring.GetGaiaTeamID()
	local enemyTeams = {}
	local alliedTeams = {}
	for i=1, #teams do
		local teamID = teams[i]
		if ((teamID ~= myTeamID) and (teamID ~= gaiaID) and not Spring.AreTeamsAllied(teamID, myTeamID)) then
			enemyTeams[#enemyTeams + 1] = teamID
		else
			if ((teamID ~= myTeamID) and (teamID ~= gaiaID) and Spring.AreTeamsAllied(teamID, myTeamID))then
				alliedTeams[#alliedTeams+ 1] = teamID
			end
		end
		
	end
	local AllTeams = { myTeam = myTeamID , alliedTeams = alliedTeams,enemyTeams = enemyTeams}
	
	return AllTeams 
end