local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end



--return function(AtlasUnits, UnitsToRescue )
return function (pair)
		--[[  
		TaskFields:
			Task.SavedUnits
			Task.DangerousUnits
			Task.DeadUnits
			Task.UnitsToRescue 
			report.GroupDead
		--]]

		
		local mAtlasID = pair.AtlasID
		local mUnitToRescueID = pair.UnitToRescueID
		bb[mAtlasID] = nil
		bb[mUnitToRescueID] = nil
		
		
		
end
