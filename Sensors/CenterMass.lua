

local sensorInfo = {
	name = "CenterMass",
	desc = "Return Center Mass of a groupUnits ",
	author = "DarioLanza",
	date = "2010-04-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

--************************************************     PriorityQueue     **************************************************************

local SpringGetUnitPosition = Spring.GetUnitPosition


return function( groupUnits )
	if (groupUnits == nil)then
		return nil
	end
	local totalUnits = #groupUnits
	if(totalUnits <= 0)then
		return nil
	end
	local total_x = 0
	local total_y = 0
	local total_z = 0
	for i = 1,  #groupUnits do
	if(Spring.ValidUnitID(groupUnits[i]) ~= nil and groupUnits[i]  ~= nil )then
		local x,y,z = SpringGetUnitPosition(groupUnits[i])
		if( x == nil or y == nil or z == nil)then
			total_x = total_x
		else
		total_x = total_x + x 
		total_y = total_y + y
		total_z = total_z + z
		end
	end	
	end
	
	avg_x = total_x/totalUnits
	avg_y = total_y/totalUnits
	avg_z = total_z/totalUnits
	return Vec3(avg_x,avg_y,avg_z)
	
end





