--[[function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end]]


local sensorInfo = {
	name = "loadUnitsInHovercraft",
	desc = "Returns list of enemy units",
	author = "DarioLanza",
	date = "2010-04-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description load units in an Area
-- @argument listOfUnits [array of unitIDs] unfiltered list 
-- @argument x number
-- @argument z number
-- @argument radius number






return function(unitBought)
	local lanes = {}
	local info = Sensors.core.missionInfo()
	topPoints =  info.corridors.Top.points
	for index,point in pairs (topPoints) do
		if ( point.isStrongPoint == true )then
			if(point.ownerAllyID == 0) then
				tempStrongPoint = index
			else
				break
			end
		end
	end
	local firstLane = { myUnits = {} , HasRadar = false , furhterSP = tempStrongPoint}
	
	lanes = { firstLane = firstLane , secondLane = firstLane,thirdLane = firstLane}
	return lanes
	--local firstLane  = lanes.firstLane
	--local secondLane = lanes.secondLane
	--local thirdLane  = lanes.thirdLane
	
end





