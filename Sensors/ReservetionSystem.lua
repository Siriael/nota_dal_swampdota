local sensorInfo = {
	name = "Form_NGroups",
	desc = "Split in N groups",
	author = "Dario Lanza",
	date = "2017-11-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end



--return function(AtlasUnits, UnitsToRescue )
return function ()
		--[[  
		TaskFields:
			Task.SavedUnits
			Task.DangerousUnits
			Task.DeadUnits
			Task.UnitsToRescue 
			report.GroupDead
		--]]
		local AtlasUnits = bb.ListGroups.atlas
		local Radars = bb.ListGroups.radars
		
		local missionInfo = Sensors.core.MissionInfo()
		for indexAtlas,mAtlasID in pairs(AtlasUnits)do
			if(bb[mAtlasID] == nil and Spring.ValidUnitID(mAtlasID) )then
				for indexUnitToRescue,mUnitToRescueID in pairs (Radars )do
					local pointX, pointY, pointZ = Spring.GetUnitPosition(mUnitToRescueID)
					local myposition = Vec3(pointX, pointY, pointZ)
					if (bb[mUnitToRescueID] == nil and Spring.ValidUnitID(mUnitToRescueID) and myposition:Distance(bb.CP.pos)>= 2000 )then
							bb[mAtlasID] = mUnitToRescueID
							bb[mUnitToRescueID] = mAtlasID
							PairedUnits = { AtlasID = mAtlasID , UnitToRescueID = mUnitToRescueID }
							return PairedUnits 
						
					end
				end
			end
		end
		
		--if(bb.CP.index >= 70)then
		local Boxes = bb.ListGroups.boxes
			for indexAtlas,mAtlasID in pairs(AtlasUnits)do
				if(bb[mAtlasID] == nil and Spring.ValidUnitID(mAtlasID) )then
					for indexUnitToRescue,mUnitToRescueID in pairs (Boxes )do
						local pointX, pointY, pointZ = Spring.GetUnitPosition(mUnitToRescueID)
						local myposition = Vec3(pointX, pointY, pointZ)
						if (bb[mUnitToRescueID] == nil and Spring.ValidUnitID(mUnitToRescueID) and myposition:Distance(bb.CP.pos)>= 1000 )then
								bb[mAtlasID] = mUnitToRescueID
								bb[mUnitToRescueID] = mAtlasID
								PairedUnits = { AtlasID = mAtlasID , UnitToRescueID = mUnitToRescueID , boxes = true}
								return PairedUnits 
							
						end
					end
				end
			end
		
end
