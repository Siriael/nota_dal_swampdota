
-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Buy unit in param",
		parameterDefs = {
			{ 
				name = "unitName",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "'armbox'",
			},
		}
	}
end

function Run(self, units, parameter)
   if (not self.haveorders or (Spring.GetCommandQueue(units[1], 0) < 1)) then
			for i=1, #units do
				
				SpringGiveOrderToUnit(units[i], cmdID, {closestDeadUnitSpot.x, closestDeadUnitSpot.y, closestDeadUnitSpot.z, 2*halfWidth}, {})
				
			end
			self.haveorders = true
		end
	
	return SUCCESS
end


function Reset(self)
	return self
end
