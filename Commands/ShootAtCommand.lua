
-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Buy unit in param",
		parameterDefs = {
			{ 
				name = "arty",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "'armbox'",
			},
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "'armbox'",
			},
			{ 
				name = "enemyID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "'armbox'",
			},
			{ 
				name = "mode",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "'armbox'",
			}
		}
	}
end
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition
function Run(self, units, parameter)
	local group = parameter.arty
	local position = parameter.position
	local enemyID = parameter.enemyID
	local cmdID = CMD.ATTACK
	local cmdMODE = parameter.mode
	local radius = 50
	local temp =  Spring.GetCommandQueue(group[1])
	
	for index,UnitID in pairs (group)do
		SpringGiveOrderToUnit(UnitID, CMD.FIRE_STATE , {cmdMODE},  {'shift'})
		--SpringGiveOrderToUnit(UnitID, cmdID, {position.x,position.y,position.z, radius},  {})
		Spring.GiveOrderToUnit(UnitID, cmdID, {enemyID},  {})
	end
	
	return SUCCESS
end


function Reset(self)
	return self
end
